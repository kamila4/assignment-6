package com.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/login")
public class LoginFilter extends HttpFilter implements Filter {
       
    /**
     * @see HttpFilter#HttpFilter()
     */
    public LoginFilter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		
		String[] creds = {"kamila@trainee.nrifintech.com","12345"};
		HttpServletRequest req = (HttpServletRequest)request;
		
		String email = (String) req.getParameter("email");
		String pass = (String) req.getParameter("pass");
		System.out.println(email);
		
		if(creds[0]==email && creds[1]==pass) {
			
			
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}else if(email==null||pass==null) {
			PrintWriter out = response.getWriter();
			out.println("One or more feilds cannot be empty");
		}else {
			PrintWriter out = response.getWriter();
			out.println("Invalid Login Credentials");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
